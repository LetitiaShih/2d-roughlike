﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	public float levelStartDelay=2f;
	public float turnDelay=.1f;
	public int playerFoodPoints=100;
	public static GameManager instance = null;	//Static instance of GameManager which allows it to be accessed by any other script.
	//Declaring it as static means that the variable will belong to the class itself as opposed to an instance of the class.
	
	private Text levelText;
	private GameObject levelImage;
	private BoardManager boardScript;
	[HideInInspector]public bool playersTurn=true;
	private int level=1;
	private List<Enemy> enemies;
	private bool enemiesMove;
	private bool firstRun=true;
	private bool doingSetup=true;
	


	// Use this for initialization
	void Awake()
	{
		if(instance==null)
		instance=this;

		else if(instance!=this)
		Destroy(gameObject);

		DontDestroyOnLoad(gameObject);
		Debug.Log("Don't Destroy On Load");

		enemies=new List<Enemy>();

		boardScript = GetComponent<BoardManager>();

		InitGame();
	} 
	void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
	{
		if(firstRun){
			firstRun=false;
			return;
		}
		
		level++;
		InitGame();
	}
	void OnEnable(){
		//Tell our"OnLevelFinishedLoading"function to start listening for a scene change event as soon as this script is enabled.
		SceneManager.sceneLoaded+=OnLevelFinishedLoading;
	}
	void OnDisable(){
		//Tell our"OnLevelFinishedLoading"function to stop listening for a scene change event as soon as this script is disabled.
		//Remember to always have an unsubscription for every delegate you subscribe to!
		SceneManager.sceneLoaded-=OnLevelFinishedLoading;
	}
	void InitGame()
	{
		doingSetup=true;
		levelImage=GameObject.Find("LevelImage");
		levelText=GameObject.Find("LevelText").GetComponent<Text>();
		levelText.text="Day "+level;
		levelImage.SetActive(true);
		Invoke("HideLevelImage",levelStartDelay);
		enemies.Clear();
		boardScript.SetupScene(level);
	//	Debug.Log("Enter InitGame");

	}
	public void GameOver()
	{
		levelText.fontSize=24;
		levelText.text="After "+level+" days, you starved.";
		levelImage.SetActive(true);
		enabled=false;
	}
	private void HideLevelImage(){
		levelImage.SetActive(false);
		doingSetup=false;
	}
	void Update(){
		if(playersTurn||enemiesMove||doingSetup)
		return;
			
		StartCoroutine(MoveEnemies());
	}
	public void AddEnemyToList(Enemy script)
	{
		//Add Enemy to List enemies.
		enemies.Add(script);
	}
	IEnumerator MoveEnemies(){
		enemiesMove=true;
		yield return new WaitForSeconds(turnDelay);
		if(enemies.Count==0){
			yield return new WaitForSeconds(turnDelay);
		}
		for(int i=0;i<enemies.Count;i++){
			enemies[i].MoveEnemy();
			yield return new WaitForSeconds(enemies[i].moveTime);
		}
		playersTurn=true;
		enemiesMove=false;
	}

}

