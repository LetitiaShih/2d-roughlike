﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour {

	public Sprite dmgSprite;
	public int hp=3;
	public AudioClip chopSound1;
	public AudioClip chopSound2;	

	private SpriteRenderer spriteRenderer;
	// Use this for initialization
	void Awake () {
		spriteRenderer=GetComponent<SpriteRenderer>();
		//FindObjectOfType<Player>().enabled = true;
//		 FindObjectOfType<Player>().gameObject.SetActive(true);

	}
	
	public void DamageWall(int loss){
		SoundManager.instance.RandomizeSfx(chopSound1,chopSound2);
		spriteRenderer.sprite=dmgSprite;
	//	Debug.Log("wall damaged");
		hp-=loss;
		if(hp<=0){
			gameObject.SetActive(false);
		}
	}
}
