﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random; 	


	public class BoardManager : MonoBehaviour 
	{

		[Serializable]
		public class Count{
			public int minimum;
			public int maximum;
			public Count(int min, int max){
				minimum=min;
				maximum=max;
			}
		}
		public int rows=8;
		public int columns=8;
		public Count wallCount=new Count(5,9);
		public Count foodCount=new Count(1,5);
		public GameObject[] foodTiles;
		public GameObject[] EnemyTiles;
		public GameObject[] wallTiles;
		public GameObject[] outerWallsTiles;
		public GameObject[] floorTiles;
		public GameObject exit;

		private Transform boardHolder;
		private List <Vector3> gridPositions=new List<Vector3>();

		void InitialiseList(){
			gridPositions.Clear();

			for(int i=1;i<rows-1;i++){
				for(int j=1; j<columns-1;j++){
					gridPositions.Add(new Vector3(i,j,0f));
				}
			}
		}
		void BoardSetUp(){
			boardHolder=new GameObject("Board").transform;
			//place floor
			for(int x=-1;x<columns+1;x++){
				for(int y=-1;y<rows+1;y++){ 
					GameObject toInstantiate = floorTiles[Random.Range (0,floorTiles.Length)];
					
					if(x==-1||x==columns||y==-1||y==rows)
					toInstantiate = outerWallsTiles [Random.Range (0, outerWallsTiles.Length)];

					GameObject instance= Instantiate(toInstantiate,new Vector3(x,y,0f),Quaternion.identity) as GameObject;

					instance.transform.SetParent(boardHolder);
					

				}
			}

		}

		Vector3 RandomPosition(){

			int randomIndex=Random.Range(0,gridPositions.Count);

			Vector3 randomPosition = gridPositions[randomIndex];

			gridPositions.RemoveAt (randomIndex);

			return randomPosition;

		}
		void LayoutObjectAtRandom(GameObject[] tileArray, int minimum, int maximum){
			int objectCount = Random.Range (minimum, maximum+1);

			for(int i=0;i<objectCount;i++){
				Vector3 randomPosition = RandomPosition();

				GameObject randomTilePick = tileArray[Random.Range (0, tileArray.Length)];

				Instantiate(randomTilePick,randomPosition,Quaternion.identity);
				
			}
		}
			public void SetupScene(int level){
			InitialiseList();
			BoardSetUp();
			LayoutObjectAtRandom(wallTiles,wallCount.minimum,wallCount.maximum);
			LayoutObjectAtRandom(foodTiles,foodCount.maximum,foodCount.maximum);
			int enemyCount = (int)Mathf.Log(level, 2f);
			LayoutObjectAtRandom(EnemyTiles,enemyCount,enemyCount);
			
			Instantiate(exit,new Vector3(columns-1,rows-1,0f),Quaternion.identity);

		}

	}

