﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public AudioSource musicSource;
	public AudioSource efxSource;
	public float fastPitch=1.05f;
	public float slowPitch=0.95f;
	
	public static SoundManager instance=null;
	void Start () {
		if(instance==null){
			instance=this;
		}
		else if(instance!=this)
		Destroy(gameObject);
		DontDestroyOnLoad (gameObject);
	}
	
		public void PlaySingle(AudioClip clip)
		{
			//Set the clip of our efxSource audio source to the clip passed in as a parameter.
			efxSource.clip = clip;
			
			//Play the clip.
			efxSource.Play ();
		}
	public void RandomizeSfx(params AudioClip[] clips){
		int randomIndex = Random.Range(0, clips.Length-1);
		float randomPitch=Random.Range(slowPitch,fastPitch);
			efxSource.pitch = randomPitch;
			
			//Set the clip to the clip at our randomly chosen index.
			efxSource.clip = clips[randomIndex];
			
			//Play the clip.
			efxSource.Play();
	}
}
